## Installation

Use the package manager [composer](https://pip.pypa.io/en/stable/) to install foobar.

```bash
composer install
```

Use the package manager [yarn](https://pip.pypa.io/en/stable/) to install foobar.

```bash
yarn install
```

## Start

[symfony](https://pip.pypa.io/en/stable/) start.
```bash
symfony server:start
```

[yarn](https://pip.pypa.io/en/stable/) start.

```bash
yarn run encore dev --watch
```
## Enjoy
