import React, {Component} from 'react';
import axios from 'axios';
import Read from "./Read.js";
import Add from "./Add.js";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

class Client extends Component {


    constructor() {
        super();
        this.state = {clients: [], loading: true}

        this.deleteSoft = this.deleteSoft.bind(this);

    }

    componentDidMount() {
        this.getPosts();
    }

    getPosts() {
        axios.get(`https://app.tacbox.fr/api/customers`).then(res => {
            console.log(res.data["hydra:member"])
            const clients = res.data["hydra:member"]
            this.setState({clients , loading: false})
        })
    }

    deleteSoft(e, id) {
        e.preventDefault()
        axios.get(`https://app.tacbox.fr/api/customers/${id}`)
            .then(res => {
                    let etat = res.data.note
                console.log(etat)
                    this.setState(
                        {etat : "ryytty"})

            },
            (error) => { console.log(error.message) })
    }

    handleClick() {
        axios.get(`https://app.tacbox.fr/api/customers/19`).then(res => {
            const clients = res.data["hydra:member"]
            this.setState({clients, loading: false})
        })
    }

    render() {
        const loading = this.state.loading;
        return (
            <div>
                <section className="row-section">
                    <div className="container">
                        <div className="row">
                            <h2 className="text-center"><span>List of client</span>Created with <i
                                className="fa fa-heart"></i>React and symfony Webpack</h2>
                        </div>
                        <Link className={"nav-link"} to={"/add"}><i className="fas fa-plus-square"></i></Link>
                        {this.state.clients.map(client =>
                            <div className="col-md-10 offset-md-1 row-block" key={client.id}>
                                {client.etat ?
                                <ul id="sortable">
                                    <li>
                                        <div className="media">
                                            <div className="media-body">
                                                <h4>{client.id}</h4>
                                                <p>{client.firstName}</p>
                                                <p>{client.lastName}</p>
                                                <p>{client.email}</p>
                                                <p>{client.mobile}</p>
                                                <p>{client.company}</p>
                                            </div>
                                            <div className="media-body">
                                                <h4>Action</h4>
                                                <Link
                                                    to={`/read/${client.id}`}
                                                    className="badge badge-primary"
                                                >Read</Link>
                                                <Link
                                                    to={`/edit/${client.id}`}
                                                    className="badge badge-warning"
                                                >Edit</Link>
                                                <Link
                                                    to={`/delete/${client.id}`}
                                                    className="badge badge-danger"
                                                >Delete</Link>
                                            </div>
                                        </div>
                                    </li>

                                </ul>
                            : <h1></h1>}
                            </div>
                        )}
                    </div>
                </section>
                <Router>
                <Switch>
                    <Route path="/read/:id" component={Read} />
                    <Route path="/add">
                        <Add></Add>
                    </Route>

                </Switch>
                </Router>
            </div>
        )
    }
}
export default Client;