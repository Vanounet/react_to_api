import React, {Component} from 'react';
import axios from 'axios';

class Add extends Component {
    constructor(props) {
        super(props);
        this.state = {
                customerType: "/api/customer_types/1",
                company: '',
                companyID: "",
                website: "",
                tel: "",
                email: "",
                mobile: "",
                password: "",
                firstName: "",
                lastName: "",
                hasAccount: true,
                notes: "",
                billingType: "/api/billing_types/1",
                bankAccounts: [],
                fax: "",
                gender: "/api/genders/2",
                birthDate: "2021-03-22T00:00:00+01:00",
                etat: true
        };
        this.changeHandler= this.changeHandler.bind(this)
    }


    handleSubmit(e) {
        e.preventDefault();
        console.log(this.state);

        axios.post(`https://app.tacbox.fr/api/customers`, this.state )
            .then(res => {

                console.log(res.data["hydra:member"]);
            },
            (error) => { console.log(error.message) })
    }

    changeHandler(e) {
        this.setState(({[e.target.name]: e.target.value}))
    }

    render() {
        const {company, companyID, website, tel, email, mobile, password,firstName, lastName } = this.state
        return (
            <div className="row">
                <div className="col-sm-4 offset-sm-4">
                <form onSubmit={e => this.handleSubmit(e)}>
                    <div className="form-group">
                        <label>
                            company:
                            <input type="text" className="form-control" name="company" value={company} onChange={this.changeHandler} />
                        </label>
                    </div>
                    <div className="form-group">
                        <label>
                            companyID:
                            <input type="text" className="form-control" name="companyID" value={companyID} onChange={this.changeHandler} />
                        </label>
                    </div>
                    <div className="form-group">
                        <label>
                            website:
                            <input type="text" className="form-control" name="website" value={website} onChange={this.changeHandler} />
                        </label>
                    </div>
                    <div className="form-group">
                        <label>
                            website:
                            <input type="text" className="form-control" name="tel" value={tel} onChange={this.changeHandler} />
                        </label>
                    </div>
                    <div className="form-group">
                        <label>
                            email:
                            <input type="text" className="form-control" name="email" value={email} onChange={this.changeHandler} />
                        </label>
                    </div>

                    <div className="form-group">
                        <label>
                            mobile
                            <input type="text" className="form-control" name="mobile" value={mobile} onChange={this.changeHandler} />
                        </label>
                    </div>

                    <div className="form-group">
                        <label>
                            website:
                            <input type="text" className="form-control" name="password" value={password} onChange={this.changeHandler} />
                        </label>
                    </div>

                    <div className="form-group">
                        <label>
                            firstname:
                            <input type="text" className="form-control" name="firstName" value={firstName} onChange={this.changeHandler} />
                        </label>
                    </div>
                    <div className="form-group">
                        <label>
                            lastname:
                            <input type="text" className="form-control" name="lastName" value={lastName} onChange={this.changeHandler} />
                        </label>
                    </div>
                    <button type="submit" className="btn btn-primary">Add</button>
                </form>
                </div>
            </div>

        )
    }
}

export default Add