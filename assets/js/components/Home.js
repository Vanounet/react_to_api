import React, {Component} from 'react';
import {Route, Switch,Redirect, Link, withRouter} from 'react-router-dom';
import Users from './User.js';
import Posts from './Posts.js';
import Client from './Client.js'
import Add from "./Add";
import Read from "./Read.js";
import Edit from "./Edit.js";
import Delete from "./Delete.js";


class Home extends Component {

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <Link className={"navbar-brand"} to={"/"}> Symfony React Project </Link>
                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link className={"nav-link"} to={"/posts"}> Posts </Link>
                            </li>

                            <li className="nav-item">
                                <Link className={"nav-link"} to={"/users"}> Users </Link>
                            </li>
                            <li className="nav-item">
                                <Link className={"nav-link"} to={"/client"}> client </Link>
                            </li>
                        </ul>
                    </div>
                </nav>
                <Switch>
                    <Redirect exact from="/" to="/users" />
                    <Route path="/users" component={Users} />
                    <Route path="/posts" component={Posts} />
                    <Route path="/client" component={Client} />
                    <Route path="/edit/:id" component={Edit} />
                    <Route path="/delete/:id" component={Delete} />
                    <Route path="/add">
                        <Add></Add>
                    </Route>
                    <Route path="/read/:id" component={Read} />
                </Switch>
            </div>
        )
    }
}

export default Home;