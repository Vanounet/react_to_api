import React, {Component} from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";

class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customerType: "/api/customer_types/1",
            company: '',
            companyID: "",
            website: "",
            tel: "",
            email: "",
            mobile: "",
            password: "",
            firstName: "",
            lastName: "",
            hasAccount: true,
            notes: "",
            billingType: "/api/billing_types/1",
            bankAccounts: [],
            fax: "",
            gender: "/api/genders/2",
            birthDate: "2021-03-22T00:00:00+01:00",
            etat: false
        };
        this.changeHandler= this.changeHandler.bind(this)
    }
    componentDidMount() {
        this.getDetail()
    }

    deleteSoft(e) {
        e.preventDefault();

        const id = this.props.match.params.id;
        axios.put(`https://app.tacbox.fr/api/customers/${id}`, this.state  )
            .then(res => {
                const client = res.data
                const etat = res.data.etat
                    this.setState({ client, etat: false })
                    console.log(client);
                },
                (error) => { console.log(error.message) })
    }

    getDetail() {
        const id = this.props.match.params.id;
        console.log(this.props.match)
        console.log(id)
        axios.get(`https://app.tacbox.fr/api/customers/${id}` )
            .then(res => {
                    this.setState({
                        customerType: "/api/customer_types/1",
                        company: res.data.company,
                        companyID: res.data.companyID,
                        website: res.data.website,
                        tel: res.data.tel,
                        email: res.data.email,
                        mobile: res.data.mobile,
                        password: res.data.password,
                        firstName: res.data.firstName,
                        lastName: res.data.lastName,
                        hasAccount: true,
                        notes: "",
                        billingType: "/api/billing_types/1",
                        bankAccounts: [],
                        fax: res.data.fax,
                        gender: "/api/genders/2",
                        birthDate: "2021-03-22T00:00:00+01:00",
                        etat: true
                    }, () => {console.log(this.state)}),
                        console.log(res);
                },
                (error) => { console.log(error.message) })
    }


    changeHandler(e) {
        this.setState(({[e.target.name]: e.target.value}))
    }

    render() {
        const {company, companyID, website, tel, email, mobile, password,firstName, lastName } = this.state
        return (
            <div className="row">
                <form onSubmit={e => this.deleteSoft(e)}>
                    <button type="submit" className="btn btn-primary">Delete</button>
                </form>
                <Link to={"/client"} className="btn btn-primary">BackURL</Link>
            </div>
        )
    }
}

export default Edit