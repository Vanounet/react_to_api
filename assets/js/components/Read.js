import React, {Component} from 'react';
import axios from 'axios';
import {Link, Redirect, Route, Switch} from "react-router-dom";
import Add from "./Add.js";

class Read extends Component {

    constructor() {
        super();
        this.state = {clients: [], loading: true}
    }

    componentDidMount() {

        this.getPosts();
    }

    getPosts() {
        let id = this.props.match.params.id
        axios.get(`https://app.tacbox.fr/api/customers/`+ id).then(res => {
            console.log(res.data)
            const clients = res.data
            this.setState({clients, loading: false})
        })
    }

    render() {
        const loading = this.state.loading;
        const client = this.state.clients;
        return (
            <div>
                <h1>Read</h1>
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">{client.id}</h5>
                        <p className="card-text">{client.firstName}</p>
                        <p className="card-text">{client.lastName}</p>
                        <p className="card-text">{client.email}</p>
                        <p className="card-text">{client.mobile}</p>
                        <p className="card-text">{client.company}</p>
                        <Link to={"/client"} className="btn btn-primary">BackURL</Link>
                    </div>
                </div>
            </div>
        )
    }
}
export default Read